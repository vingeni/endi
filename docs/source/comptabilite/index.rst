Comptabilité
============

Export des écritures
---------------------

.. include:: export_comptable.rst

Fichiers PDF accessibles dans CAERP
-----------------------------------

.. include:: depot_document_comptable_pdf.rst
