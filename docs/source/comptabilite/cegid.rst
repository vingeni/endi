Export des écritures au format Cegid
=======================================

CAERP permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Cegid.

Afin de configurer CAERP pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:caerp]" du fichier .ini

.. code-block::

    caerp.services.treasury_invoice_writer=caerp.export.cegid.InvoiceWriter

    caerp.services.treasury_payment_writer=caerp.export.cegid.PaymentWriter

    caerp.services.treasury_expense_writer=caerp.export.cegid.ExpenseWriter

    caerp.services.treasury_supplier_invoice_writer=caerp.export.cegid.SupplierInvoiceWriter

    caerp.services.treasury_supplier_payment_writer=caerp.export.cegid.SupplierPaymentWriter

    caerp.services.treasury_expense_payment_writer=caerp.export.cegid.ExpensePaymentWriter
