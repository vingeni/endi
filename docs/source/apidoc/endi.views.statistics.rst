caerp.views.statistics package
=============================

Submodules
----------

caerp.views.statistics.rest\_api module
--------------------------------------

.. automodule:: caerp.views.statistics.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.statistics.routes module
-----------------------------------

.. automodule:: caerp.views.statistics.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.statistics.statistics module
---------------------------------------

.. automodule:: caerp.views.statistics.statistics
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.statistics
   :members:
   :undoc-members:
   :show-inheritance:
