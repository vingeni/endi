caerp\_base package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp_base.models
   caerp_base.utils

Submodules
----------

caerp\_base.consts module
------------------------

.. automodule:: caerp_base.consts
   :members:
   :undoc-members:
   :show-inheritance:

caerp\_base.exception module
---------------------------

.. automodule:: caerp_base.exception
   :members:
   :undoc-members:
   :show-inheritance:

caerp\_base.mail module
----------------------

.. automodule:: caerp_base.mail
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp_base
   :members:
   :undoc-members:
   :show-inheritance:
