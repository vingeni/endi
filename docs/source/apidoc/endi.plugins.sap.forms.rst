caerp.plugins.sap.forms package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap.forms.admin
   caerp.plugins.sap.forms.tasks

Submodules
----------

caerp.plugins.sap.forms.attestation module
-----------------------------------------

.. automodule:: caerp.plugins.sap.forms.attestation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap.forms.nova module
----------------------------------

.. automodule:: caerp.plugins.sap.forms.nova
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap.forms
   :members:
   :undoc-members:
   :show-inheritance:
