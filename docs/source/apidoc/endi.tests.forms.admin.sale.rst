caerp.tests.forms.admin.sale package
===================================

Submodules
----------

caerp.tests.forms.admin.sale.test\_tva module
--------------------------------------------

.. automodule:: caerp.tests.forms.admin.sale.test_tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
