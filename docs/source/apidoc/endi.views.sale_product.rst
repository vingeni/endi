caerp.views.sale\_product package
================================

Submodules
----------

caerp.views.sale\_product.rest\_api module
-----------------------------------------

.. automodule:: caerp.views.sale_product.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.sale\_product.routes module
--------------------------------------

.. automodule:: caerp.views.sale_product.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.sale\_product.sale\_product module
---------------------------------------------

.. automodule:: caerp.views.sale_product.sale_product
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
