caerp.tests.views.third\_party.customer package
==============================================

Submodules
----------

caerp.tests.views.third\_party.customer.test\_controller module
--------------------------------------------------------------

.. automodule:: caerp.tests.views.third_party.customer.test_controller
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.third\_party.customer.test\_customer module
------------------------------------------------------------

.. automodule:: caerp.tests.views.third_party.customer.test_customer
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.third\_party.customer.test\_rest\_api module
-------------------------------------------------------------

.. automodule:: caerp.tests.views.third_party.customer.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.third_party.customer
   :members:
   :undoc-members:
   :show-inheritance:
