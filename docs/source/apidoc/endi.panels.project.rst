caerp.panels.project package
===========================

Submodules
----------

caerp.panels.project.business\_metrics\_mixins module
----------------------------------------------------

.. automodule:: caerp.panels.project.business_metrics_mixins
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.project.phase module
--------------------------------

.. automodule:: caerp.panels.project.phase
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.project.type module
-------------------------------

.. automodule:: caerp.panels.project.type
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.panels.project
   :members:
   :undoc-members:
   :show-inheritance:
