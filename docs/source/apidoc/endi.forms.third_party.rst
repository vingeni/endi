caerp.forms.third\_party package
===============================

Submodules
----------

caerp.forms.third\_party.base module
-----------------------------------

.. automodule:: caerp.forms.third_party.base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.third\_party.customer module
---------------------------------------

.. automodule:: caerp.forms.third_party.customer
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.third\_party.supplier module
---------------------------------------

.. automodule:: caerp.forms.third_party.supplier
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.third_party
   :members:
   :undoc-members:
   :show-inheritance:
