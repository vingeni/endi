caerp.views.admin.expense package
================================

Submodules
----------

caerp.views.admin.expense.accounting module
------------------------------------------

.. automodule:: caerp.views.admin.expense.accounting
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.expense.numbers module
---------------------------------------

.. automodule:: caerp.views.admin.expense.numbers
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.expense.types module
-------------------------------------

.. automodule:: caerp.views.admin.expense.types
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.expense
   :members:
   :undoc-members:
   :show-inheritance:
