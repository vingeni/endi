caerp.views.admin.accompagnement package
=======================================

Submodules
----------

caerp.views.admin.accompagnement.activities module
-------------------------------------------------

.. automodule:: caerp.views.admin.accompagnement.activities
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.accompagnement.competence module
-------------------------------------------------

.. automodule:: caerp.views.admin.accompagnement.competence
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.accompagnement.workshop module
-----------------------------------------------

.. automodule:: caerp.views.admin.accompagnement.workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.accompagnement
   :members:
   :undoc-members:
   :show-inheritance:
