caerp.plugins.sap\_urssaf3p.models package
=========================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap_urssaf3p.models.services

Submodules
----------

caerp.plugins.sap\_urssaf3p.models.customer module
-------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.models.customer
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.models.payment\_request module
---------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.models.payment_request
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.models
   :members:
   :undoc-members:
   :show-inheritance:
