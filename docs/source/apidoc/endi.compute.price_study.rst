caerp.compute.price\_study package
=================================

Submodules
----------

caerp.compute.price\_study.ht\_mode module
-----------------------------------------

.. automodule:: caerp.compute.price_study.ht_mode
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.price\_study.supplier\_ht\_mode module
---------------------------------------------------

.. automodule:: caerp.compute.price_study.supplier_ht_mode
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.compute.price_study
   :members:
   :undoc-members:
   :show-inheritance:
