caerp.views.admin package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.views.admin.accompagnement
   caerp.views.admin.accounting
   caerp.views.admin.expense
   caerp.views.admin.main
   caerp.views.admin.sale
   caerp.views.admin.supplier
   caerp.views.admin.userdatas

Submodules
----------

caerp.views.admin.layout module
------------------------------

.. automodule:: caerp.views.admin.layout
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.tools module
-----------------------------

.. automodule:: caerp.views.admin.tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
