caerp.tests.models.sale\_product package
=======================================

Submodules
----------

caerp.tests.models.sale\_product.test\_services module
-----------------------------------------------------

.. automodule:: caerp.tests.models.sale_product.test_services
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.sale\_product.test\_work\_item module
-------------------------------------------------------

.. automodule:: caerp.tests.models.sale_product.test_work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
