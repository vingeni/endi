caerp.tests.views.sale\_product package
======================================

Submodules
----------

caerp.tests.views.sale\_product.test\_rest\_api module
-----------------------------------------------------

.. automodule:: caerp.tests.views.sale_product.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
