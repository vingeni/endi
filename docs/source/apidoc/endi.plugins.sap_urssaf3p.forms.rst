caerp.plugins.sap\_urssaf3p.forms package
========================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap_urssaf3p.forms.admin
   caerp.plugins.sap_urssaf3p.forms.tasks

Submodules
----------

caerp.plugins.sap\_urssaf3p.forms.customer module
------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.forms.customer
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.forms.tva module
-------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.forms.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.forms
   :members:
   :undoc-members:
   :show-inheritance:
