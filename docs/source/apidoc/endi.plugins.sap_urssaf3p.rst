caerp.plugins.sap\_urssaf3p package
==================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap_urssaf3p.forms
   caerp.plugins.sap_urssaf3p.models
   caerp.plugins.sap_urssaf3p.views

Submodules
----------

caerp.plugins.sap\_urssaf3p.api\_client module
---------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.api_client
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.celery\_jobs module
----------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.celery_jobs
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.caerp\_admin\_commands module
-------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.caerp_admin_commands
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.populate module
------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.populate
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.serializers module
---------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.serializers
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p
   :members:
   :undoc-members:
   :show-inheritance:
