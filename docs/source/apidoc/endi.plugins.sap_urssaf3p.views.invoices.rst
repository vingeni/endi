caerp.plugins.sap\_urssaf3p.views.invoices package
=================================================

Submodules
----------

caerp.plugins.sap\_urssaf3p.views.invoices.invoice module
--------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.invoices.invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap\_urssaf3p.views.invoices.lists module
------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.invoices.lists
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
