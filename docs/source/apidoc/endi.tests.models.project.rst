caerp.tests.models.project package
=================================

Submodules
----------

caerp.tests.models.project.test\_project module
----------------------------------------------

.. automodule:: caerp.tests.models.project.test_project
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.project
   :members:
   :undoc-members:
   :show-inheritance:
