caerp.tests.forms.admin package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.forms.admin.sale

Module contents
---------------

.. automodule:: caerp.tests.forms.admin
   :members:
   :undoc-members:
   :show-inheritance:
