caerp.panels package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.panels.company_index
   caerp.panels.manage
   caerp.panels.project
   caerp.panels.supply
   caerp.panels.task

Submodules
----------

caerp.panels.activity module
---------------------------

.. automodule:: caerp.panels.activity
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.files module
------------------------

.. automodule:: caerp.panels.files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.form module
-----------------------

.. automodule:: caerp.panels.form
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.indicators module
-----------------------------

.. automodule:: caerp.panels.indicators
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.menu module
-----------------------

.. automodule:: caerp.panels.menu
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.navigation module
-----------------------------

.. automodule:: caerp.panels.navigation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.sidebar module
--------------------------

.. automodule:: caerp.panels.sidebar
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.tabs module
-----------------------

.. automodule:: caerp.panels.tabs
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.third\_party module
-------------------------------

.. automodule:: caerp.panels.third_party
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.widgets module
--------------------------

.. automodule:: caerp.panels.widgets
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.workshop module
---------------------------

.. automodule:: caerp.panels.workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.panels
   :members:
   :undoc-members:
   :show-inheritance:
