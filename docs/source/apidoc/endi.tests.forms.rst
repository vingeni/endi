caerp.tests.forms package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.forms.admin
   caerp.tests.forms.project
   caerp.tests.forms.sale_product
   caerp.tests.forms.supply
   caerp.tests.forms.tasks
   caerp.tests.forms.third_party
   caerp.tests.forms.user

Submodules
----------

caerp.tests.forms.conftest module
--------------------------------

.. automodule:: caerp.tests.forms.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_accounting module
----------------------------------------

.. automodule:: caerp.tests.forms.test_accounting
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_admin module
-----------------------------------

.. automodule:: caerp.tests.forms.test_admin
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_custom\_types module
-------------------------------------------

.. automodule:: caerp.tests.forms.test_custom_types
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_expense module
-------------------------------------

.. automodule:: caerp.tests.forms.test_expense
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_files module
-----------------------------------

.. automodule:: caerp.tests.forms.test_files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_forms module
-----------------------------------

.. automodule:: caerp.tests.forms.test_forms
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_payments module
--------------------------------------

.. automodule:: caerp.tests.forms.test_payments
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_progress\_invoicing module
-------------------------------------------------

.. automodule:: caerp.tests.forms.test_progress_invoicing
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.test\_supplier\_invoice module
-----------------------------------------------

.. automodule:: caerp.tests.forms.test_supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms
   :members:
   :undoc-members:
   :show-inheritance:
