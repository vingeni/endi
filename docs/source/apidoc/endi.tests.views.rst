caerp.tests.views package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.views.accounting
   caerp.tests.views.admin
   caerp.tests.views.estimations
   caerp.tests.views.expenses
   caerp.tests.views.internal_invoicing
   caerp.tests.views.invoices
   caerp.tests.views.price_study
   caerp.tests.views.progress_invoicing
   caerp.tests.views.project
   caerp.tests.views.sale_product
   caerp.tests.views.statistics
   caerp.tests.views.supply
   caerp.tests.views.task
   caerp.tests.views.third_party
   caerp.tests.views.user
   caerp.tests.views.userdatas

Submodules
----------

caerp.tests.views.test\_activity module
--------------------------------------

.. automodule:: caerp.tests.views.test_activity
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_auth module
----------------------------------

.. automodule:: caerp.tests.views.test_auth
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_base module
----------------------------------

.. automodule:: caerp.tests.views.test_base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_commercial module
----------------------------------------

.. automodule:: caerp.tests.views.test_commercial
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_company module
-------------------------------------

.. automodule:: caerp.tests.views.test_company
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_csv\_import module
-----------------------------------------

.. automodule:: caerp.tests.views.test_csv_import
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_forms\_holiday module
--------------------------------------------

.. automodule:: caerp.tests.views.test_forms_holiday
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_forms\_project module
--------------------------------------------

.. automodule:: caerp.tests.views.test_forms_project
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_forms\_utils module
------------------------------------------

.. automodule:: caerp.tests.views.test_forms_utils
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_forms\_validator module
----------------------------------------------

.. automodule:: caerp.tests.views.test_forms_validator
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_holiday module
-------------------------------------

.. automodule:: caerp.tests.views.test_holiday
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_treasury\_files module
---------------------------------------------

.. automodule:: caerp.tests.views.test_treasury_files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.test\_workshop module
--------------------------------------

.. automodule:: caerp.tests.views.test_workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views
   :members:
   :undoc-members:
   :show-inheritance:
