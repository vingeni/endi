caerp.forms.training package
===========================

Submodules
----------

caerp.forms.training.bpf module
------------------------------

.. automodule:: caerp.forms.training.bpf
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.training.trainer module
----------------------------------

.. automodule:: caerp.forms.training.trainer
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.training.training module
-----------------------------------

.. automodule:: caerp.forms.training.training
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.training
   :members:
   :undoc-members:
   :show-inheritance:
