caerp.tests.views.project package
================================

Submodules
----------

caerp.tests.views.project.test\_phase module
-------------------------------------------

.. automodule:: caerp.tests.views.project.test_phase
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.project.test\_project module
---------------------------------------------

.. automodule:: caerp.tests.views.project.test_project
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.project.test\_rest\_api module
-----------------------------------------------

.. automodule:: caerp.tests.views.project.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.project
   :members:
   :undoc-members:
   :show-inheritance:
