caerp.tests.views.user package
=============================

Submodules
----------

caerp.tests.views.user.test\_company module
------------------------------------------

.. automodule:: caerp.tests.views.user.test_company
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.user.test\_login module
----------------------------------------

.. automodule:: caerp.tests.views.user.test_login
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.user.test\_user module
---------------------------------------

.. automodule:: caerp.tests.views.user.test_user
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.user
   :members:
   :undoc-members:
   :show-inheritance:
