caerp.views.userdatas package
============================

Submodules
----------

caerp.views.userdatas.career\_path module
----------------------------------------

.. automodule:: caerp.views.userdatas.career_path
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.userdatas.filelist module
------------------------------------

.. automodule:: caerp.views.userdatas.filelist
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.userdatas.lists module
---------------------------------

.. automodule:: caerp.views.userdatas.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.userdatas.py3o module
--------------------------------

.. automodule:: caerp.views.userdatas.py3o
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.userdatas.routes module
----------------------------------

.. automodule:: caerp.views.userdatas.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.userdatas.userdatas module
-------------------------------------

.. automodule:: caerp.views.userdatas.userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.userdatas
   :members:
   :undoc-members:
   :show-inheritance:
