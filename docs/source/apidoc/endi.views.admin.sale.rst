caerp.views.admin.sale package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.views.admin.sale.accounting
   caerp.views.admin.sale.business_cycle
   caerp.views.admin.sale.forms
   caerp.views.admin.sale.pdf

Submodules
----------

caerp.views.admin.sale.catalog module
------------------------------------

.. automodule:: caerp.views.admin.sale.catalog
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.index module
----------------------------------

.. automodule:: caerp.views.admin.sale.index
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.internal\_invoicing\_numbers module
---------------------------------------------------------

.. automodule:: caerp.views.admin.sale.internal_invoicing_numbers
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.numbers module
------------------------------------

.. automodule:: caerp.views.admin.sale.numbers
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.receipts module
-------------------------------------

.. automodule:: caerp.views.admin.sale.receipts
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.tva module
--------------------------------

.. automodule:: caerp.views.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
