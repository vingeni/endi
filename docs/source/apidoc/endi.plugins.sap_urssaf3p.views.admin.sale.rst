caerp.plugins.sap\_urssaf3p.views.admin.sale package
===================================================

Submodules
----------

caerp.plugins.sap\_urssaf3p.views.admin.sale.tva module
------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
