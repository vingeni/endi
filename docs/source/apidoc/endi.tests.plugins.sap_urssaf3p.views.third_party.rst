caerp.tests.plugins.sap\_urssaf3p.views.third\_party package
===========================================================

Submodules
----------

caerp.tests.plugins.sap\_urssaf3p.views.third\_party.test\_customer module
-------------------------------------------------------------------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.views.third_party.test_customer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.views.third_party
   :members:
   :undoc-members:
   :show-inheritance:
