caerp.tests.models.user package
==============================

Submodules
----------

caerp.tests.models.user.test\_login module
-----------------------------------------

.. automodule:: caerp.tests.models.user.test_login
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.user.test\_user module
----------------------------------------

.. automodule:: caerp.tests.models.user.test_user
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.user.test\_userdatas module
---------------------------------------------

.. automodule:: caerp.tests.models.user.test_userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.user
   :members:
   :undoc-members:
   :show-inheritance:
