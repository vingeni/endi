caerp.tests.views.admin.sale package
===================================

Submodules
----------

caerp.tests.views.admin.sale.test\_accounting module
---------------------------------------------------

.. automodule:: caerp.tests.views.admin.sale.test_accounting
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.sale.test\_forms module
----------------------------------------------

.. automodule:: caerp.tests.views.admin.sale.test_forms
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.sale.test\_tva module
--------------------------------------------

.. automodule:: caerp.tests.views.admin.sale.test_tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
