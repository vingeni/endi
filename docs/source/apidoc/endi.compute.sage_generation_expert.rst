caerp.compute.sage\_generation\_expert package
=============================================

Submodules
----------

caerp.compute.sage\_generation\_expert.compute module
----------------------------------------------------

.. automodule:: caerp.compute.sage_generation_expert.compute
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.compute.sage_generation_expert
   :members:
   :undoc-members:
   :show-inheritance:
