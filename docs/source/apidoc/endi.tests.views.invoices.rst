caerp.tests.views.invoices package
=================================

Submodules
----------

caerp.tests.views.invoices.conftest module
-----------------------------------------

.. automodule:: caerp.tests.views.invoices.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.invoices.test\_invoice module
----------------------------------------------

.. automodule:: caerp.tests.views.invoices.test_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.invoices.test\_rest\_api module
------------------------------------------------

.. automodule:: caerp.tests.views.invoices.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
