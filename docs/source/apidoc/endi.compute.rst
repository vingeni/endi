caerp.compute package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.compute.price_study
   caerp.compute.sage
   caerp.compute.sage_generation_expert
   caerp.compute.sale_product
   caerp.compute.task

Submodules
----------

caerp.compute.base\_line module
------------------------------

.. automodule:: caerp.compute.base_line
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.expense module
---------------------------

.. automodule:: caerp.compute.expense
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.math\_utils module
-------------------------------

.. automodule:: caerp.compute.math_utils
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.parser module
--------------------------

.. automodule:: caerp.compute.parser
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.supplier\_invoice module
-------------------------------------

.. automodule:: caerp.compute.supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.compute.supplier\_order module
-----------------------------------

.. automodule:: caerp.compute.supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.compute
   :members:
   :undoc-members:
   :show-inheritance:
