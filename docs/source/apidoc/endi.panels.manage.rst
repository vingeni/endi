caerp.panels.manage package
==========================

Submodules
----------

caerp.panels.manage.activities module
------------------------------------

.. automodule:: caerp.panels.manage.activities
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.manage.expenses module
----------------------------------

.. automodule:: caerp.panels.manage.expenses
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.manage.suppliers module
-----------------------------------

.. automodule:: caerp.panels.manage.suppliers
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.manage.tasks module
-------------------------------

.. automodule:: caerp.panels.manage.tasks
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.panels.manage
   :members:
   :undoc-members:
   :show-inheritance:
