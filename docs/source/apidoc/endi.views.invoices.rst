caerp.views.invoices package
===========================

Submodules
----------

caerp.views.invoices.cancelinvoice module
----------------------------------------

.. automodule:: caerp.views.invoices.cancelinvoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.invoices.invoice module
----------------------------------

.. automodule:: caerp.views.invoices.invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.invoices.lists module
--------------------------------

.. automodule:: caerp.views.invoices.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.invoices.rest\_api module
------------------------------------

.. automodule:: caerp.views.invoices.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.invoices.routes module
---------------------------------

.. automodule:: caerp.views.invoices.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
