caerp.i18n package
=================

Submodules
----------

caerp.i18n.translater module
---------------------------

.. automodule:: caerp.i18n.translater
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.i18n
   :members:
   :undoc-members:
   :show-inheritance:
