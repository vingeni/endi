caerp.plugins.sap\_urssaf3p.views.admin.sap package
==================================================

Submodules
----------

caerp.plugins.sap\_urssaf3p.views.admin.sap.avance\_immediate module
-------------------------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.admin.sap.avance_immediate
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.admin.sap
   :members:
   :undoc-members:
   :show-inheritance:
