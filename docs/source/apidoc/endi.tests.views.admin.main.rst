caerp.tests.views.admin.main package
===================================

Submodules
----------

caerp.tests.views.admin.main.test\_cae module
--------------------------------------------

.. automodule:: caerp.tests.views.admin.main.test_cae
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.main.test\_contact module
------------------------------------------------

.. automodule:: caerp.tests.views.admin.main.test_contact
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.main.test\_digital\_signatures module
------------------------------------------------------------

.. automodule:: caerp.tests.views.admin.main.test_digital_signatures
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.main.test\_file\_type module
---------------------------------------------------

.. automodule:: caerp.tests.views.admin.main.test_file_type
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.admin.main.test\_site module
---------------------------------------------

.. automodule:: caerp.tests.views.admin.main.test_site
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.admin.main
   :members:
   :undoc-members:
   :show-inheritance:
