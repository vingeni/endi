caerp.views.estimations package
==============================

Submodules
----------

caerp.views.estimations.estimation module
----------------------------------------

.. automodule:: caerp.views.estimations.estimation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.estimations.lists module
-----------------------------------

.. automodule:: caerp.views.estimations.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.estimations.rest\_api module
---------------------------------------

.. automodule:: caerp.views.estimations.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.estimations.routes module
------------------------------------

.. automodule:: caerp.views.estimations.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.estimations
   :members:
   :undoc-members:
   :show-inheritance:
