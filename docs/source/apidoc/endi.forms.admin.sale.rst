caerp.forms.admin.sale package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.forms.admin.sale.business_cycle

Submodules
----------

caerp.forms.admin.sale.bookeeping module
---------------------------------------

.. automodule:: caerp.forms.admin.sale.bookeeping
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.sale.form\_options module
------------------------------------------

.. automodule:: caerp.forms.admin.sale.form_options
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.sale.insurance module
--------------------------------------

.. automodule:: caerp.forms.admin.sale.insurance
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.sale.mentions module
-------------------------------------

.. automodule:: caerp.forms.admin.sale.mentions
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.sale.naming module
-----------------------------------

.. automodule:: caerp.forms.admin.sale.naming
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.sale.tva module
--------------------------------

.. automodule:: caerp.forms.admin.sale.tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
