caerp.consts package
===================

Submodules
----------

caerp.consts.insee\_countries module
-----------------------------------

.. automodule:: caerp.consts.insee_countries
   :members:
   :undoc-members:
   :show-inheritance:

caerp.consts.insee\_departments module
-------------------------------------

.. automodule:: caerp.consts.insee_departments
   :members:
   :undoc-members:
   :show-inheritance:

caerp.consts.street\_types module
--------------------------------

.. automodule:: caerp.consts.street_types
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.consts
   :members:
   :undoc-members:
   :show-inheritance:
