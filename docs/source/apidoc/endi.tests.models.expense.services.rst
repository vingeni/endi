caerp.tests.models.expense.services package
==========================================

Submodules
----------

caerp.tests.models.expense.services.test\_expense\_types module
--------------------------------------------------------------

.. automodule:: caerp.tests.models.expense.services.test_expense_types
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.expense.services
   :members:
   :undoc-members:
   :show-inheritance:
