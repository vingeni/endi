caerp.plugins.sap.models.task package
====================================

Submodules
----------

caerp.plugins.sap.models.task.tasks module
-----------------------------------------

.. automodule:: caerp.plugins.sap.models.task.tasks
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap.models.task
   :members:
   :undoc-members:
   :show-inheritance:
