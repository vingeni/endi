caerp.views.third\_party package
===============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.views.third_party.customer
   caerp.views.third_party.supplier

Module contents
---------------

.. automodule:: caerp.views.third_party
   :members:
   :undoc-members:
   :show-inheritance:
