caerp.views.workshops package
============================

Submodules
----------

caerp.views.workshops.export module
----------------------------------

.. automodule:: caerp.views.workshops.export
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.workshops.lists module
---------------------------------

.. automodule:: caerp.views.workshops.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.workshops.workshop module
------------------------------------

.. automodule:: caerp.views.workshops.workshop
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.workshops
   :members:
   :undoc-members:
   :show-inheritance:
