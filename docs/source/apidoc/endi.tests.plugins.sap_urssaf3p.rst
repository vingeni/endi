caerp.tests.plugins.sap\_urssaf3p package
========================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.plugins.sap_urssaf3p.views

Submodules
----------

caerp.tests.plugins.sap\_urssaf3p.conftest module
------------------------------------------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.plugins.sap\_urssaf3p.test\_forms module
---------------------------------------------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.test_forms
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.plugins.sap\_urssaf3p.test\_models module
----------------------------------------------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.test_models
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.plugins.sap\_urssaf3p.test\_serializers module
---------------------------------------------------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p.test_serializers
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.plugins.sap_urssaf3p
   :members:
   :undoc-members:
   :show-inheritance:
