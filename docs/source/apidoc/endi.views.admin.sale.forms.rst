caerp.views.admin.sale.forms package
===================================

Submodules
----------

caerp.views.admin.sale.forms.fields module
-----------------------------------------

.. automodule:: caerp.views.admin.sale.forms.fields
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.forms.insurance module
--------------------------------------------

.. automodule:: caerp.views.admin.sale.forms.insurance
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.forms.main module
---------------------------------------

.. automodule:: caerp.views.admin.sale.forms.main
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.sale.forms.mentions module
-------------------------------------------

.. automodule:: caerp.views.admin.sale.forms.mentions
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.sale.forms
   :members:
   :undoc-members:
   :show-inheritance:
