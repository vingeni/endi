caerp.tests.views.price\_study package
=====================================

Submodules
----------

caerp.tests.views.price\_study.test\_rest\_api module
----------------------------------------------------

.. automodule:: caerp.tests.views.price_study.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.price_study
   :members:
   :undoc-members:
   :show-inheritance:
