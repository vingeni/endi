caerp.forms.admin package
========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.forms.admin.main
   caerp.forms.admin.sale

Submodules
----------

caerp.forms.admin.career\_stage module
-------------------------------------

.. automodule:: caerp.forms.admin.career_stage
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.admin.expense\_type module
-------------------------------------

.. automodule:: caerp.forms.admin.expense_type
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.admin
   :members:
   :undoc-members:
   :show-inheritance:
