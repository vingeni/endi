caerp.views.files package
========================

Submodules
----------

caerp.views.files.rest\_api module
---------------------------------

.. automodule:: caerp.views.files.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.files.routes module
------------------------------

.. automodule:: caerp.views.files.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.files.views module
-----------------------------

.. automodule:: caerp.views.files.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.files
   :members:
   :undoc-members:
   :show-inheritance:
