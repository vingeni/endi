caerp.models.training package
============================

Submodules
----------

caerp.models.training.bpf module
-------------------------------

.. automodule:: caerp.models.training.bpf
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.training.trainer module
-----------------------------------

.. automodule:: caerp.models.training.trainer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.models.training
   :members:
   :undoc-members:
   :show-inheritance:
