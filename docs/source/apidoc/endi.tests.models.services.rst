caerp.tests.models.services package
==================================

Submodules
----------

caerp.tests.models.services.conftest module
------------------------------------------

.. automodule:: caerp.tests.models.services.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.services.test\_business module
------------------------------------------------

.. automodule:: caerp.tests.models.services.test_business
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.services.test\_business\_bpf module
-----------------------------------------------------

.. automodule:: caerp.tests.models.services.test_business_bpf
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.services.test\_business\_status module
--------------------------------------------------------

.. automodule:: caerp.tests.models.services.test_business_status
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.services.test\_project module
-----------------------------------------------

.. automodule:: caerp.tests.models.services.test_project
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.services.test\_sale\_file\_requirements module
----------------------------------------------------------------

.. automodule:: caerp.tests.models.services.test_sale_file_requirements
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.services
   :members:
   :undoc-members:
   :show-inheritance:
