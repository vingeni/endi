caerp.plugins.sap\_urssaf3p.views.admin package
==============================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap_urssaf3p.views.admin.sale
   caerp.plugins.sap_urssaf3p.views.admin.sap

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
