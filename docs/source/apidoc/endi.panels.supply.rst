caerp.panels.supply package
==========================

Submodules
----------

caerp.panels.supply.supplier\_invoice\_list module
-------------------------------------------------

.. automodule:: caerp.panels.supply.supplier_invoice_list
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.supply.supplier\_order\_list module
-----------------------------------------------

.. automodule:: caerp.panels.supply.supplier_order_list
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.panels.supply
   :members:
   :undoc-members:
   :show-inheritance:
