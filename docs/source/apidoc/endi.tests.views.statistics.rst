caerp.tests.views.statistics package
===================================

Submodules
----------

caerp.tests.views.statistics.test\_rest\_api module
--------------------------------------------------

.. automodule:: caerp.tests.views.statistics.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.statistics
   :members:
   :undoc-members:
   :show-inheritance:
