caerp.panels.task package
========================

Submodules
----------

caerp.panels.task.file\_tab module
---------------------------------

.. automodule:: caerp.panels.task.file_tab
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.task.html module
----------------------------

.. automodule:: caerp.panels.task.html
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.task.pdf module
---------------------------

.. automodule:: caerp.panels.task.pdf
   :members:
   :undoc-members:
   :show-inheritance:

caerp.panels.task.task\_list module
----------------------------------

.. automodule:: caerp.panels.task.task_list
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.panels.task
   :members:
   :undoc-members:
   :show-inheritance:
