caerp.tests.models.supply package
================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.models.supply.services

Submodules
----------

caerp.tests.models.supply.test\_internalsupplier\_invoice module
---------------------------------------------------------------

.. automodule:: caerp.tests.models.supply.test_internalsupplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.supply.test\_supplier\_invoice module
-------------------------------------------------------

.. automodule:: caerp.tests.models.supply.test_supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.supply.test\_supplier\_order module
-----------------------------------------------------

.. automodule:: caerp.tests.models.supply.test_supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models.supply
   :members:
   :undoc-members:
   :show-inheritance:
