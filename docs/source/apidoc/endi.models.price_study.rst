caerp.models.price\_study package
================================

Submodules
----------

caerp.models.price\_study.base module
------------------------------------

.. automodule:: caerp.models.price_study.base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.chapter module
---------------------------------------

.. automodule:: caerp.models.price_study.chapter
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.discount module
----------------------------------------

.. automodule:: caerp.models.price_study.discount
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.price\_study module
--------------------------------------------

.. automodule:: caerp.models.price_study.price_study
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.product module
---------------------------------------

.. automodule:: caerp.models.price_study.product
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.services module
----------------------------------------

.. automodule:: caerp.models.price_study.services
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.work module
------------------------------------

.. automodule:: caerp.models.price_study.work
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.price\_study.work\_item module
------------------------------------------

.. automodule:: caerp.models.price_study.work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.models.price_study
   :members:
   :undoc-members:
   :show-inheritance:
