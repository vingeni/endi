caerp.tests.panels package
=========================

Submodules
----------

caerp.tests.panels.test\_company module
--------------------------------------

.. automodule:: caerp.tests.panels.test_company
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.panels.test\_menu module
-----------------------------------

.. automodule:: caerp.tests.panels.test_menu
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.panels
   :members:
   :undoc-members:
   :show-inheritance:
