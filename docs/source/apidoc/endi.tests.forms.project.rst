caerp.tests.forms.project package
================================

Submodules
----------

caerp.tests.forms.project.test\_project module
---------------------------------------------

.. automodule:: caerp.tests.forms.project.test_project
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms.project
   :members:
   :undoc-members:
   :show-inheritance:
