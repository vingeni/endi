caerp.views.project package
==========================

Submodules
----------

caerp.views.project.business module
----------------------------------

.. automodule:: caerp.views.project.business
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.controller module
------------------------------------

.. automodule:: caerp.views.project.controller
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.estimation module
------------------------------------

.. automodule:: caerp.views.project.estimation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.files module
-------------------------------

.. automodule:: caerp.views.project.files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.invoice module
---------------------------------

.. automodule:: caerp.views.project.invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.layout module
--------------------------------

.. automodule:: caerp.views.project.layout
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.lists module
-------------------------------

.. automodule:: caerp.views.project.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.phase module
-------------------------------

.. automodule:: caerp.views.project.phase
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.project module
---------------------------------

.. automodule:: caerp.views.project.project
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.rest\_api module
-----------------------------------

.. automodule:: caerp.views.project.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.project.routes module
--------------------------------

.. automodule:: caerp.views.project.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.project
   :members:
   :undoc-members:
   :show-inheritance:
