caerp.plugins.sap\_urssaf3p.forms.tasks package
==============================================

Submodules
----------

caerp.plugins.sap\_urssaf3p.forms.tasks.invoice module
-----------------------------------------------------

.. automodule:: caerp.plugins.sap_urssaf3p.forms.tasks.invoice
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap_urssaf3p.forms.tasks
   :members:
   :undoc-members:
   :show-inheritance:
