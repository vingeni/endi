caerp.tests.views.userdatas package
==================================

Submodules
----------

caerp.tests.views.userdatas.test\_py3o module
--------------------------------------------

.. automodule:: caerp.tests.views.userdatas.test_py3o
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.views.userdatas.test\_userdatas module
-------------------------------------------------

.. automodule:: caerp.tests.views.userdatas.test_userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.userdatas
   :members:
   :undoc-members:
   :show-inheritance:
