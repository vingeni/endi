caerp.tests package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.compute
   caerp.tests.caerp_celery
   caerp.tests.caerp_payment
   caerp.tests.forms
   caerp.tests.models
   caerp.tests.panels
   caerp.tests.plugins
   caerp.tests.utils
   caerp.tests.views

Submodules
----------

caerp.tests.base module
----------------------

.. automodule:: caerp.tests.base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.conftest module
--------------------------

.. automodule:: caerp.tests.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.test\_session module
-------------------------------

.. automodule:: caerp.tests.test_session
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.tools module
-----------------------

.. automodule:: caerp.tests.tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests
   :members:
   :undoc-members:
   :show-inheritance:
