caerp.views.task package
=======================

Submodules
----------

caerp.views.task.pdf\_rendering\_service module
----------------------------------------------

.. automodule:: caerp.views.task.pdf_rendering_service
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.task.pdf\_storage\_service module
--------------------------------------------

.. automodule:: caerp.views.task.pdf_storage_service
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.task.rest\_api module
--------------------------------

.. automodule:: caerp.views.task.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.task.utils module
----------------------------

.. automodule:: caerp.views.task.utils
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.task.views module
----------------------------

.. automodule:: caerp.views.task.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.task
   :members:
   :undoc-members:
   :show-inheritance:
