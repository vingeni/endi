caerp.tests.views.third\_party package
=====================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.views.third_party.customer

Module contents
---------------

.. automodule:: caerp.tests.views.third_party
   :members:
   :undoc-members:
   :show-inheritance:
