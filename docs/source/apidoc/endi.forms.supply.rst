caerp.forms.supply package
=========================

Submodules
----------

caerp.forms.supply.supplier\_invoice module
------------------------------------------

.. automodule:: caerp.forms.supply.supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.supply.supplier\_order module
----------------------------------------

.. automodule:: caerp.forms.supply.supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.supply
   :members:
   :undoc-members:
   :show-inheritance:
