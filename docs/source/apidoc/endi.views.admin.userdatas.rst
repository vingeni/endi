caerp.views.admin.userdatas package
==================================

Submodules
----------

caerp.views.admin.userdatas.career\_stage module
-----------------------------------------------

.. automodule:: caerp.views.admin.userdatas.career_stage
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.userdatas.options module
-----------------------------------------

.. automodule:: caerp.views.admin.userdatas.options
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.userdatas.templates module
-------------------------------------------

.. automodule:: caerp.views.admin.userdatas.templates
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.userdatas
   :members:
   :undoc-members:
   :show-inheritance:
