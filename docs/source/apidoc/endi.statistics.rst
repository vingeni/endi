caerp.statistics package
=======================

Submodules
----------

caerp.statistics.filter\_options module
--------------------------------------

.. automodule:: caerp.statistics.filter_options
   :members:
   :undoc-members:
   :show-inheritance:

caerp.statistics.inspect module
------------------------------

.. automodule:: caerp.statistics.inspect
   :members:
   :undoc-members:
   :show-inheritance:

caerp.statistics.query\_helper module
------------------------------------

.. automodule:: caerp.statistics.query_helper
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.statistics
   :members:
   :undoc-members:
   :show-inheritance:
