caerp.views.third\_party.customer package
========================================

Submodules
----------

caerp.views.third\_party.customer.base module
--------------------------------------------

.. automodule:: caerp.views.third_party.customer.base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.third\_party.customer.controller module
--------------------------------------------------

.. automodule:: caerp.views.third_party.customer.controller
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.third\_party.customer.lists module
---------------------------------------------

.. automodule:: caerp.views.third_party.customer.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.third\_party.customer.rest\_api module
-------------------------------------------------

.. automodule:: caerp.views.third_party.customer.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.third\_party.customer.routes module
----------------------------------------------

.. automodule:: caerp.views.third_party.customer.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.third\_party.customer.views module
---------------------------------------------

.. automodule:: caerp.views.third_party.customer.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.third_party.customer
   :members:
   :undoc-members:
   :show-inheritance:
