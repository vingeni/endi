caerp.forms.sale\_product package
================================

Submodules
----------

caerp.forms.sale\_product.category module
----------------------------------------

.. automodule:: caerp.forms.sale_product.category
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.sale\_product.sale\_product module
---------------------------------------------

.. automodule:: caerp.forms.sale_product.sale_product
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.sale\_product.work module
------------------------------------

.. automodule:: caerp.forms.sale_product.work
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
