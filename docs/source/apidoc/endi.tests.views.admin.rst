caerp.tests.views.admin package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.views.admin.accompagnement
   caerp.tests.views.admin.expense
   caerp.tests.views.admin.main
   caerp.tests.views.admin.sale
   caerp.tests.views.admin.userdatas

Submodules
----------

caerp.tests.views.admin.test\_tools module
-----------------------------------------

.. automodule:: caerp.tests.views.admin.test_tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
