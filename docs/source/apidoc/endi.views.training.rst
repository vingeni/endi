caerp.views.training package
===========================

Submodules
----------

caerp.views.training.business\_bpf module
----------------------------------------

.. automodule:: caerp.views.training.business_bpf
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.training.dashboard module
------------------------------------

.. automodule:: caerp.views.training.dashboard
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.training.lists module
--------------------------------

.. automodule:: caerp.views.training.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.training.routes module
---------------------------------

.. automodule:: caerp.views.training.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.training.trainer module
----------------------------------

.. automodule:: caerp.views.training.trainer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.training
   :members:
   :undoc-members:
   :show-inheritance:
