caerp.views.supply package
=========================

Submodules
----------

caerp.views.supply.rest\_api module
----------------------------------

.. automodule:: caerp.views.supply.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.supply.supplier\_invoice module
------------------------------------------

.. automodule:: caerp.views.supply.supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.supply.supplier\_order module
----------------------------------------

.. automodule:: caerp.views.supply.supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.supply
   :members:
   :undoc-members:
   :show-inheritance:
