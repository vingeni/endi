caerp.tests.views.internal\_invoicing package
============================================

Submodules
----------

caerp.tests.views.internal\_invoicing.test\_views module
-------------------------------------------------------

.. automodule:: caerp.tests.views.internal_invoicing.test_views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.internal_invoicing
   :members:
   :undoc-members:
   :show-inheritance:
