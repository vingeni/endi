caerp.views.admin.main.companies package
=======================================

Submodules
----------

caerp.views.admin.main.companies.companies\_label module
-------------------------------------------------------

.. automodule:: caerp.views.admin.main.companies.companies_label
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.main.companies.company\_activities module
----------------------------------------------------------

.. automodule:: caerp.views.admin.main.companies.company_activities
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.admin.main.companies.internal\_companies module
----------------------------------------------------------

.. automodule:: caerp.views.admin.main.companies.internal_companies
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.admin.main.companies
   :members:
   :undoc-members:
   :show-inheritance:
