caerp.tests.forms.sale\_product package
======================================

Submodules
----------

caerp.tests.forms.sale\_product.test\_sale\_product module
---------------------------------------------------------

.. automodule:: caerp.tests.forms.sale_product.test_sale_product
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
