caerp.tests.forms.user package
=============================

Submodules
----------

caerp.tests.forms.user.test\_company module
------------------------------------------

.. automodule:: caerp.tests.forms.user.test_company
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.user.test\_login module
----------------------------------------

.. automodule:: caerp.tests.forms.user.test_login
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.user.test\_user module
---------------------------------------

.. automodule:: caerp.tests.forms.user.test_user
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.user.test\_userdatas module
--------------------------------------------

.. automodule:: caerp.tests.forms.user.test_userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms.user
   :members:
   :undoc-members:
   :show-inheritance:
