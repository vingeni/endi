caerp.utils.notification package
===============================

Submodules
----------

caerp.utils.notification.abstract module
---------------------------------------

.. automodule:: caerp.utils.notification.abstract
   :members:
   :undoc-members:
   :show-inheritance:

caerp.utils.notification.career\_path module
-------------------------------------------

.. automodule:: caerp.utils.notification.career_path
   :members:
   :undoc-members:
   :show-inheritance:

caerp.utils.notification.channels module
---------------------------------------

.. automodule:: caerp.utils.notification.channels
   :members:
   :undoc-members:
   :show-inheritance:

caerp.utils.notification.notification module
-------------------------------------------

.. automodule:: caerp.utils.notification.notification
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.utils.notification
   :members:
   :undoc-members:
   :show-inheritance:
