caerp.views.price\_study package
===============================

Submodules
----------

caerp.views.price\_study.rest\_api module
----------------------------------------

.. automodule:: caerp.views.price_study.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.price\_study.routes module
-------------------------------------

.. automodule:: caerp.views.price_study.routes
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.price\_study.utils module
------------------------------------

.. automodule:: caerp.views.price_study.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.price_study
   :members:
   :undoc-members:
   :show-inheritance:
