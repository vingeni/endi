caerp.tests.plugins package
==========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.plugins.sap
   caerp.tests.plugins.sap_urssaf3p

Module contents
---------------

.. automodule:: caerp.tests.plugins
   :members:
   :undoc-members:
   :show-inheritance:
