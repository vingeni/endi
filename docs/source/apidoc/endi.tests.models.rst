caerp.tests.models package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.tests.models.accounting
   caerp.tests.models.expense
   caerp.tests.models.price_study
   caerp.tests.models.progress_invoicing
   caerp.tests.models.project
   caerp.tests.models.sale_product
   caerp.tests.models.services
   caerp.tests.models.supply
   caerp.tests.models.task
   caerp.tests.models.third_party
   caerp.tests.models.user

Submodules
----------

caerp.tests.models.conftest module
---------------------------------

.. automodule:: caerp.tests.models.conftest
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_activity module
---------------------------------------

.. automodule:: caerp.tests.models.test_activity
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_company module
--------------------------------------

.. automodule:: caerp.tests.models.test_company
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_config module
-------------------------------------

.. automodule:: caerp.tests.models.test_config
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_files module
------------------------------------

.. automodule:: caerp.tests.models.test_files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_form\_options module
--------------------------------------------

.. automodule:: caerp.tests.models.test_form_options
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_status module
-------------------------------------

.. automodule:: caerp.tests.models.test_status
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.models.test\_tva module
----------------------------------

.. automodule:: caerp.tests.models.test_tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.models
   :members:
   :undoc-members:
   :show-inheritance:
