caerp.sql\_compute package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.sql_compute.task

Module contents
---------------

.. automodule:: caerp.sql_compute
   :members:
   :undoc-members:
   :show-inheritance:
