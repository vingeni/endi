caerp.tests.forms.supply package
===============================

Submodules
----------

caerp.tests.forms.supply.test\_init module
-----------------------------------------

.. automodule:: caerp.tests.forms.supply.test_init
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.supply.test\_supplier\_invoice module
------------------------------------------------------

.. automodule:: caerp.tests.forms.supply.test_supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.tests.forms.supply.test\_supplier\_order module
----------------------------------------------------

.. automodule:: caerp.tests.forms.supply.test_supplier_order
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.forms.supply
   :members:
   :undoc-members:
   :show-inheritance:
