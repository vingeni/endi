caerp.views.notification package
===============================

Submodules
----------

caerp.views.notification.rest\_api module
----------------------------------------

.. automodule:: caerp.views.notification.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.notification.routes module
-------------------------------------

.. automodule:: caerp.views.notification.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.notification
   :members:
   :undoc-members:
   :show-inheritance:
