caerp.forms.price\_study package
===============================

Submodules
----------

caerp.forms.price\_study.chapter module
--------------------------------------

.. automodule:: caerp.forms.price_study.chapter
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.price\_study.common module
-------------------------------------

.. automodule:: caerp.forms.price_study.common
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.price\_study.discount module
---------------------------------------

.. automodule:: caerp.forms.price_study.discount
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.price\_study.price\_study module
-------------------------------------------

.. automodule:: caerp.forms.price_study.price_study
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.price\_study.product module
--------------------------------------

.. automodule:: caerp.forms.price_study.product
   :members:
   :undoc-members:
   :show-inheritance:

caerp.forms.price\_study.work\_item module
-----------------------------------------

.. automodule:: caerp.forms.price_study.work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.forms.price_study
   :members:
   :undoc-members:
   :show-inheritance:
