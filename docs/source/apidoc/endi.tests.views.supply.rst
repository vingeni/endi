caerp.tests.views.supply package
===============================

Submodules
----------

caerp.tests.views.supply.test\_supplier\_invoice module
------------------------------------------------------

.. automodule:: caerp.tests.views.supply.test_supplier_invoice
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.tests.views.supply
   :members:
   :undoc-members:
   :show-inheritance:
