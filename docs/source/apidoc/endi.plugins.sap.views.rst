caerp.plugins.sap.views package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap.views.admin
   caerp.plugins.sap.views.estimations
   caerp.plugins.sap.views.invoices

Submodules
----------

caerp.plugins.sap.views.attestation module
-----------------------------------------

.. automodule:: caerp.plugins.sap.views.attestation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap.views.mixins module
------------------------------------

.. automodule:: caerp.plugins.sap.views.mixins
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap.views.nova module
----------------------------------

.. automodule:: caerp.plugins.sap.views.nova
   :members:
   :undoc-members:
   :show-inheritance:

caerp.plugins.sap.views.payment module
-------------------------------------

.. automodule:: caerp.plugins.sap.views.payment
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.plugins.sap.views
   :members:
   :undoc-members:
   :show-inheritance:
