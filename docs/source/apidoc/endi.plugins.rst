caerp.plugins package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   caerp.plugins.sap
   caerp.plugins.sap_urssaf3p
   caerp.plugins.solo

Module contents
---------------

.. automodule:: caerp.plugins
   :members:
   :undoc-members:
   :show-inheritance:
