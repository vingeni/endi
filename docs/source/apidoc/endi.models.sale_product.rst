caerp.models.sale\_product package
=================================

Submodules
----------

caerp.models.sale\_product.base module
-------------------------------------

.. automodule:: caerp.models.sale_product.base
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.category module
-----------------------------------------

.. automodule:: caerp.models.sale_product.category
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.sale\_product module
----------------------------------------------

.. automodule:: caerp.models.sale_product.sale_product
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.services module
-----------------------------------------

.. automodule:: caerp.models.sale_product.services
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.training module
-----------------------------------------

.. automodule:: caerp.models.sale_product.training
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.work module
-------------------------------------

.. automodule:: caerp.models.sale_product.work
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.sale\_product.work\_item module
-------------------------------------------

.. automodule:: caerp.models.sale_product.work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.models.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
