caerp.views.status package
=========================

Submodules
----------

caerp.views.status.rest\_api module
----------------------------------

.. automodule:: caerp.views.status.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.status.utils module
------------------------------

.. automodule:: caerp.views.status.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.status
   :members:
   :undoc-members:
   :show-inheritance:
