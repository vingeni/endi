caerp.models.user package
========================

Submodules
----------

caerp.models.user.group module
-----------------------------

.. automodule:: caerp.models.user.group
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.user.login module
-----------------------------

.. automodule:: caerp.models.user.login
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.user.user module
----------------------------

.. automodule:: caerp.models.user.user
   :members:
   :undoc-members:
   :show-inheritance:

caerp.models.user.userdatas module
---------------------------------

.. automodule:: caerp.models.user.userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.models.user
   :members:
   :undoc-members:
   :show-inheritance:
