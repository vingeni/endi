caerp.views.business package
===========================

Submodules
----------

caerp.views.business.business module
-----------------------------------

.. automodule:: caerp.views.business.business
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.estimation module
-------------------------------------

.. automodule:: caerp.views.business.estimation
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.expense module
----------------------------------

.. automodule:: caerp.views.business.expense
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.files module
--------------------------------

.. automodule:: caerp.views.business.files
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.invoice module
----------------------------------

.. automodule:: caerp.views.business.invoice
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.layout module
---------------------------------

.. automodule:: caerp.views.business.layout
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.lists module
--------------------------------

.. automodule:: caerp.views.business.lists
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.py3o module
-------------------------------

.. automodule:: caerp.views.business.py3o
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.rest\_api module
------------------------------------

.. automodule:: caerp.views.business.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

caerp.views.business.routes module
---------------------------------

.. automodule:: caerp.views.business.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: caerp.views.business
   :members:
   :undoc-members:
   :show-inheritance:
