Passer caerp en mode Solo
=========================

Depuis une instance caerp existante, il est possible de conserver uniquement les données de vente d'une enseigne.

.. code-block:: console

    caerp-company-export development.ini company <id de l'object Company>


Il est ensuite possible de modifier l'interface afin qu'elle soit plus adaptée à l'usage dans un cadre hors CAE.


Dans le fichier .ini de l'application rajouter :

.. code-block:: console

    caerp.modules =

    caerp.includes =
            caerp.plugins.solo

* La première clé de configuration caerp.modules vient indiquer qu'on ne veut aucun des *modules optionnels* d'CAERP.

Cependant selon les cas certains modules peuvent être nécessaires ou intéressants, il faut alors les spécifier manuellement. Voilà une suggestion de modules qui peuvent être inclus de base pour tous les utilisateurs solo :

.. code-block:: console

    caerp.modules=
        caerp.views.price_study
        caerp.views.progress_invoicing
        caerp.views.supply
        caerp.views.third_party.supplier
        caerp.views.export.supplier_invoice
        caerp.views.export.supplier_payment


* La deuxième clé vient configurer un plugin 'solo', à inclure après coup, qui va essentiellement modifier l'organisation des menus.

cf aussi :doc:`decoupage`.
