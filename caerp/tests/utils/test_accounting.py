import datetime


def set_config_value(request, key, value):
    from caerp.models.config import Config

    Config.set(key, value)
    request.config[key] = value


def test_financial_year_utils(csrf_request_with_db_and_user):
    from caerp.utils.accounting import (
        get_financial_year_data,
        get_current_financial_year_data,
        get_previous_financial_year_data,
        get_current_financial_year_value,
        get_all_financial_year_values,
    )

    today = datetime.date.today()

    # AVEC EXERCICE COMPTABLE CALÉ SUR L'ANNÉE CIVILE (config par défaut)

    fy_data = get_financial_year_data(2021)
    assert fy_data["label"] == "2021"
    assert fy_data["start_date"] == datetime.date(2021, 1, 1)

    fy_data = get_current_financial_year_data()
    assert fy_data["start_date"] == datetime.date(today.year, 1, 1)

    fy_data = get_previous_financial_year_data()
    assert fy_data["end_date"] == datetime.date(today.year - 1, 12, 31)

    assert get_current_financial_year_value() == today.year
    assert get_all_financial_year_values(csrf_request_with_db_and_user) == [today.year]

    # AVEC EXERCICE COMPTABLE DÉCALÉ
    set_config_value(csrf_request_with_db_and_user, "accounting_closure_day", 30)
    set_config_value(csrf_request_with_db_and_user, "accounting_closure_month", 9)

    fy_data = get_financial_year_data(2021)
    assert fy_data["label"] == "2020/21"
    assert fy_data["start_date"] == datetime.date(2020, 10, 1)

    fy_data = get_current_financial_year_data()
    if today.month > 9:
        assert fy_data["start_date"] == datetime.date(today.year, 10, 1)
        assert fy_data["end_date"] == datetime.date(today.year + 1, 9, 30)
    else:
        assert fy_data["start_date"] == datetime.date(today.year - 1, 10, 1)
        assert fy_data["end_date"] == datetime.date(today.year, 9, 30)

    fy_data = get_previous_financial_year_data()
    if today.month > 9:
        assert fy_data["start_date"] == datetime.date(today.year - 1, 10, 1)
        assert fy_data["end_date"] == datetime.date(today.year, 9, 30)
    else:
        assert fy_data["start_date"] == datetime.date(today.year - 2, 10, 1)
        assert fy_data["end_date"] == datetime.date(today.year - 1, 9, 30)

    if today.month > 9:
        assert get_current_financial_year_value() == today.year + 1
        assert get_all_financial_year_values(csrf_request_with_db_and_user) == [
            today.year + 1
        ]
    else:
        assert get_current_financial_year_value() == today.year
        assert get_all_financial_year_values(csrf_request_with_db_and_user) == [
            today.year
        ]


def test_get_customer_accounting_general_account(
    csrf_request_with_db_and_user, customer, customer2, tva10, tva20
):
    from caerp.utils.accounting import get_customer_accounting_general_account

    request = csrf_request_with_db_and_user

    # CONFIG
    customer.compte_cg = "411CLIENT1"
    customer.company.general_customer_account = "411ENSEIGNE1"
    customer2.compte_cg = None
    customer2.company.general_customer_account = "411ENSEIGNE2"
    customer2.company.internalgeneral_customer_account = "INT411ENSEIGNE2"
    set_config_value(request, "cae_general_customer_account", "411CAE")
    set_config_value(request, "internalcae_general_customer_account", "INT411CAE")

    # SANS COMPTES CLIENTS PAR TVA (par défaut)
    assert (
        get_customer_accounting_general_account(request, customer.id, tva10.id)
        == "411CLIENT1"
    )
    assert (
        get_customer_accounting_general_account(request, customer.id, tva20.id)
        == "411CLIENT1"
    )
    assert (
        get_customer_accounting_general_account(request, customer2.id, tva10.id)
        == "411ENSEIGNE2"
    )
    assert (
        get_customer_accounting_general_account(request, customer2.id, tva20.id)
        == "411ENSEIGNE2"
    )
    assert (
        get_customer_accounting_general_account(
            request, customer2.id, tva20.id, "internal"
        )
        == "INT411ENSEIGNE2"
    )

    # AVEC COMPTES CLIENTS PAR TVA
    set_config_value(request, "bookentry_sales_customer_account_by_tva", True)
    assert (
        get_customer_accounting_general_account(request, customer.id, tva10.id)
        == "411TVA10"
    )
    assert (
        get_customer_accounting_general_account(request, customer.id, tva20.id)
        == "411TVA20"
    )
    assert (
        get_customer_accounting_general_account(
            request, customer.id, tva20.id, "internal"
        )
        == "411TVA20"
    )
