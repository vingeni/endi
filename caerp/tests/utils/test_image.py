"""
    Test image resizing
"""
import pytest
import os
from caerp.tests.conftest import DATASDIR

from PIL import Image

from caerp.utils.image import (
    ImageResizer,
    ImageRatio,
)


@pytest.fixture
def ratio():
    return ImageRatio(5, 1)


@pytest.fixture
def resizer():
    return ImageResizer(800, 200)


def test_ratio_not_affect_equal(ratio):
    with open(os.path.join(DATASDIR, "entete5_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete5_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]
        assert Image.open(image2).size == Image.open(image).size


def test_ratio_not_affect_less(ratio):
    with open(os.path.join(DATASDIR, "entete10_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete10_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]
        assert Image.open(image2).size == Image.open(image).size


def test_ratio(ratio):
    with open(os.path.join(DATASDIR, "entete2_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete2_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]

    img_obj2 = Image.open(image2)
    width, height = img_obj2.size
    assert width / height == 5


def test_resize_width(resizer):
    with open(os.path.join(DATASDIR, "entete2_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete2_1.png", "mimetype": "image/png"}
        image2 = resizer(filedict)["fp"]

    assert Image.open(image2).size[0] == 400
    assert Image.open(image2).size[1] == 200


def test_resize_height(resizer):
    with open(os.path.join(DATASDIR, "entete5_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete5_1.png", "mimetype": "image/png"}
        image2 = resizer(filedict)["fp"]

    assert Image.open(image2).size[0] == 800
    assert Image.open(image2).size[1] == 160


def test_resize_cmyk_bug880(resizer):
    with open(os.path.join(DATASDIR, "cmyk.jpg"), "rb") as image:
        filedict = {"fp": image, "filename": "cmyk.jpg", "mimetype": "image/png"}
        result = resizer(filedict)

    assert Image.open(result["fp"]).size[0] <= 200
    assert Image.open(result["fp"]).mode == "RGB"
    assert result["filename"] == "cmyk.png"
    assert result["mimetype"] == "image/png"
