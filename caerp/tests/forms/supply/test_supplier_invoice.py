import colander
import pytest


@pytest.fixture
def supplier_wo_registration(mk_supplier):
    return mk_supplier(registration="")


def test_valid_SupplierInvoiceDispatchSchema(
    date_20190101,
    get_csrf_request_with_db,
    supplier,
    company,
    expense_type,
):
    from caerp.forms.supply.supplier_invoice import SupplierInvoiceDispatchSchema

    request = get_csrf_request_with_db()
    schema = SupplierInvoiceDispatchSchema().bind(request=request)
    data = schema.deserialize(
        {
            "date": date_20190101.isoformat(),
            "total_ht": "0",
            "total_tva": "0",
            "lines": [],
            "supplier_id": supplier.id,
            "invoice_file": {"filename": "125_1_test.pdf"},
        }
    )
    assert data == {
        "date": date_20190101,
        "invoice_file": {"filename": "125_1_test.pdf"},
        "lines": [],
        "supplier_id": supplier.id,
        "total_ht": 0,
        "total_tva": 0,
    }

    schema = SupplierInvoiceDispatchSchema().bind(request=request)
    data = schema.deserialize(
        {
            "date": date_20190101.isoformat(),
            "total_ht": "10",
            "total_tva": "2",
            "lines": [
                {
                    "ht": "7.5",
                    "tva": "1.5",
                    "description": "A",
                    "company_id": company.id,
                    "type_id": expense_type.id,
                },
                {
                    "ht": "2.5",
                    "tva": "0.5",
                    "description": "B",
                    "company_id": company.id,
                    "type_id": expense_type.id,
                },
            ],
            "supplier_id": supplier.id,
            "invoice_file": {"filename": "125_1_test.pdf"},
        }
    )
    assert data == {
        "date": date_20190101,
        "invoice_file": {"filename": "125_1_test.pdf"},
        "lines": [
            {
                "company_id": company.id,
                "description": "A",
                "ht": 750,
                "tva": 150,
                "type_id": expense_type.id,
            },
            {
                "company_id": company.id,
                "description": "B",
                "ht": 250,
                "tva": 50,
                "type_id": expense_type.id,
            },
        ],
        "supplier_id": supplier.id,
        "total_ht": 1000,
        "total_tva": 200,
    }


def test_invalid_SupplierInvoiceDispatchSchema(
    date_20190101,
    get_csrf_request_with_db,
    supplier,
    supplier_wo_registration,
    company,
    expense_type,
):
    from caerp.forms.supply.supplier_invoice import (
        SupplierInvoiceDispatchSchema,
    )

    pyramid_request = get_csrf_request_with_db()
    schema = SupplierInvoiceDispatchSchema().bind(request=pyramid_request)

    # Wrong sum
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "date": date_20190101.isoformat(),
                "total_ht": "0",
                "total_tva": "2",
                "lines": [
                    {
                        "ht": "7.5",
                        "tva": "1.5",
                        "description": "A",
                        "company_id": company.id,
                        "type_id": expense_type.id,
                    },
                    {
                        "ht": "2.5",
                        "tva": "0.5",
                        "description": "B",
                        "company_id": company.id,
                        "type_id": expense_type.id,
                    },
                ],
                "supplier_id": supplier.id,
                "invoice_file": {"filename": "125_1_test.pdf"},
            }
        )

    # Wrong supplier (no Supplier.registration)
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "date": date_20190101,
                "total_ht": "0",
                "total_tva": "0",
                "lines": [],
                "supplier_id": supplier_wo_registration.id,
                "invoice_file": {"filename": "125_1_test.pdf"},
            }
        )


def test_get_supplier_invoice_edit_schema():
    from caerp.forms.supply.supplier_invoice import (
        get_supplier_invoice_edit_schema,
    )

    schema = get_supplier_invoice_edit_schema()

    for field in (
        "date",
        "supplier_id",
        "cae_percentage",
        "payer_id",
        "supplier_orders",
        "remote_invoice_number",
    ):
        assert field in schema

    schema = get_supplier_invoice_edit_schema(internal=True)

    for field in (
        "date",
        "supplier_id",
        "cae_percentage",
        "payer_id",
        "supplier_orders",
        "remote_invoice_number",
    ):
        assert field not in schema


def test_invoice_orders_validator(
    get_csrf_request_with_db,
    mk_supplier_order,
    schema_node,
    mk_supplier,
):
    from caerp.forms.supply.supplier_invoice import deferred_invoice_orders_validator

    validator = deferred_invoice_orders_validator(
        schema_node, {"request": get_csrf_request_with_db()}
    )

    order_0 = mk_supplier_order(cae_percentage=0)
    order_0bis = mk_supplier_order(cae_percentage=0)
    order_100 = mk_supplier_order(cae_percentage=100)
    order_0_other_supplier = mk_supplier_order(supplier=mk_supplier(), cae_percentage=0)
    # assert no exception
    validator(schema_node, [order_0.id, order_0bis.id])

    with pytest.raises(colander.Invalid):
        validator(
            schema_node,
            [order_0.id, order_100.id],
        )

    with pytest.raises(colander.Invalid):
        validator(
            schema_node,
            [order_0.id, order_0_other_supplier.id],
        )


def test_supplier_invoice_edit_schema_supplier_and_order_mismatch(
    mk_supplier, supplier_invoice, supplier, supplier_order, get_csrf_request_with_db
):
    from caerp.forms.supply.supplier_invoice import (
        get_supplier_invoice_edit_schema,
    )

    pyramid_request = get_csrf_request_with_db(context=supplier_invoice)

    schema = get_supplier_invoice_edit_schema()
    schema = schema.bind(request=pyramid_request)
    toremove = [
        node.name
        for node in schema
        if node.name not in ("supplier_id", "supplier_orders")
    ]

    for key in toremove:
        del schema[key]

    # assert no exception
    schema.deserialize(
        {
            "supplier_id": supplier_invoice.supplier.id,
            "supplier_orders": [],
        }
    )

    # assert no exception
    schema.deserialize(
        {
            "supplier_id": supplier_invoice.supplier.id,
            "supplier_orders": [supplier_order.id],
        }
    )

    supplier2 = mk_supplier()
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "supplier_id": supplier2.id,
                "supplier_orders": [supplier_order.supplier.id],
            }
        )


def test_validate_supplier_invoice(
    mk_supplier,
    supplier_invoice,
    supplier,
    supplier_order,
    get_csrf_request_with_db,
    user,
):
    from caerp.forms.supply.supplier_invoice import (
        validate_supplier_invoice,
    )

    pyramid_request = get_csrf_request_with_db(context=supplier_invoice)

    for conf in (
        {"payer": None, "cae_percentage": 100},
        {"payer": user, "cae_percentage": 100},
        {"payer": user, "cae_percentage": 80},
    ):
        for key, value in conf.items():
            setattr(supplier_invoice, key, value)
        # No exception
        validate_supplier_invoice(
            supplier_invoice,
            pyramid_request,
        )

    with pytest.raises(colander.Invalid):
        supplier_invoice.payer = None
        supplier_invoice.cae_percentage = 80
        validate_supplier_invoice(supplier_invoice, pyramid_request)
