from caerp.export.sale_product import serialize_catalog


def test_serialize_empty_catalog(company):
    serialized_catalog = serialize_catalog(company)

    assert serialized_catalog.keys() == {"data", "data_format"}
    assert serialized_catalog["data"] == {
        "base_sale_products": [],
        "sale_products_works": [],
        "sale_products_trainings": [],
    }
    assert serialized_catalog["data_format"].endswith("/sales_catalog")


def test_serialize_catalog_minimal(sale_product, company):
    serialized_catalog = serialize_catalog(company)

    assert serialized_catalog["data"]["sale_products_works"] == []
    assert serialized_catalog["data"]["sale_products_trainings"] == []

    serialized = serialized_catalog["data"]["base_sale_products"]
    assert len(serialized) == 1
    assert serialized[0]["updated_at"] is not None
    del serialized[0]["updated_at"]
    del serialized[0]["created_at"]  # Won't match

    assert serialized == [
        {
            "archived": "false",
            # "created_at": "2023-11-01T17:38:37.124425",
            "description": "",
            "ht": "0",
            "id": str(sale_product.id),
            "label": "Label",
            "margin_rate": "0",
            "mode": "ht",
            "notes": None,
            "product": {
                "active": "true",
                "compte_cg": "122",
                "internal": "false",
                "name": "product",
                "urssaf_code_nature": "",
                "tva": {
                    "compte_cg": "TVA20",
                    "value": "2000",
                },
            },
            "ref": None,
            "stock_operations": [],
            "supplier_ht": "100000",
            "supplier_ref": None,
            "supplier_unity_amount": None,
            "ttc": "0",
            "tva": {
                "compte_cg": "TVA20",
                "value": "2000",
            },
            "type_": "sale_product_material",
            "unity": "",
        }
    ]


def test_serialize_catalog_full(
    company,
    mk_sale_product,
    sale_product_category,
    supplier,
    expense_type,
):
    mk_sale_product(
        company=company,
        category=sale_product_category,
        supplier=supplier,
        purchase_type=expense_type,
    )

    serialized_catalog = serialize_catalog(company)
    serialized = serialized_catalog["data"]["base_sale_products"]
    assert len(serialized) == 1

    assert serialized[0]["category"] == {
        "description": "My Category Desc",
        "id": str(sale_product_category.id),
        "title": "My Cat Title",
    }

    assert serialized[0]["purchase_type"] == {
        "label": "Base type",
    }

    assert serialized[0]["supplier"]["label"] == "Fournisseur Test"


def test_serialize_catalog_sale_product_work(
    company,
    sale_product,
    sale_product_work,
    mk_sale_product_work_item,
):
    wi1 = mk_sale_product_work_item(
        description="A", sale_product_work=sale_product_work
    )
    wi2 = mk_sale_product_work_item(
        description="B", sale_product_work=sale_product_work
    )

    serialized_catalog = serialize_catalog(company)
    assert serialized_catalog["data"]["sale_products_trainings"] == []

    base_products = serialized_catalog["data"]["base_sale_products"]
    work_products = serialized_catalog["data"]["sale_products_works"]

    assert len(base_products) == 1
    assert len(work_products) == 1

    assert base_products[0]["id"] == str(sale_product.id)
    assert work_products[0]["id"] == str(sale_product_work.id)

    assert len(work_products[0]["items"]) == 2

    assert work_products[0]["items"][1] == {
        "base_sale_product_id": str(sale_product.id),
        "description": "B",
        "_ht": None,
        "locked": "true",
        "_mode": "ht",
        "quantity": "1",
        "_supplier_ht": None,
        "total_ht": "0",
        "type_": "material",
        "_unity": None,
    }


def test_serialize_catalog_training(
    company,
    sale_product,
    sale_product_training,
):
    serialized_catalog = serialize_catalog(company)
    assert serialized_catalog["data"]["sale_products_works"] == []

    base_products = serialized_catalog["data"]["base_sale_products"]
    training_products = serialized_catalog["data"]["sale_products_trainings"]

    assert len(base_products) == 1
    assert len(training_products) == 1

    assert training_products[0]["types"] == [
        {
            "active": "true",
            "label": "Bilan de compétence",
            "order": "0",
            "type_": "training_type_options",
        }
    ]
