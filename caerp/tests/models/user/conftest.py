import datetime
import pytest

from caerp.models.career_path import CareerPath
from caerp.models.career_stage import CareerStage
from caerp.models.user.userdatas import (
    UserDatas,
    CaeSituationOption,
    UserDatasCustomFields,
    SocialStatusDatas,
)


@pytest.fixture
def mk_career_stage(fixture_factory, name="Etape", stage_type=None):
    return fixture_factory(
        CareerStage,
        name=name,
        stage_type=stage_type,
    )


@pytest.fixture
def stage_default(mk_career_stage):
    return mk_career_stage()


@pytest.fixture
def stage_entry(mk_career_stage):
    return mk_career_stage(name="Entrée", stage_type="entry")


@pytest.fixture
def stage_contract(mk_career_stage):
    return mk_career_stage(name="Contrat", stage_type="contract")


@pytest.fixture
def stage_amendment(mk_career_stage):
    return mk_career_stage(name="Avenant", stage_type="amendment")


@pytest.fixture
def stage_exit(mk_career_stage):
    return mk_career_stage(name="Sortie", stage_type="exit")


@pytest.fixture
def integre_cae_situation_option(dbsession):
    option = CaeSituationOption(label="Integre", is_integration=True)
    dbsession.add(option)
    dbsession.flush()
    return option


@pytest.fixture
def employee_cae_situation_option(dbsession):
    option = CaeSituationOption(label="Employé")
    dbsession.add(option)
    dbsession.flush()
    return option


@pytest.fixture
def member_cae_situation_option(dbsession):
    option = CaeSituationOption(label="Associé")
    dbsession.add(option)
    dbsession.flush()
    return option


@pytest.fixture
def exit_cae_situation_option(dbsession):
    option = CaeSituationOption(label="Sortie")
    dbsession.add(option)
    dbsession.flush()
    return option


@pytest.fixture
def full_career_path_1(
    dbsession,
    stage_default,
    stage_entry,
    stage_contract,
    stage_amendment,
    integre_cae_situation_option,
    employee_cae_situation_option,
    member_cae_situation_option,
):
    carreer_path = [
        CareerPath(
            start_date=datetime.date(2022, 12, 1),
            career_stage=stage_default,
            stage_type=None,
            cae_situation_id=integre_cae_situation_option.id,
        ),
        CareerPath(
            start_date=datetime.date(2022, 12, 25),
            career_stage=stage_entry,
            stage_type="entry",
            cae_situation_id=employee_cae_situation_option.id,
        ),
        CareerPath(
            start_date=datetime.date(2023, 2, 1),
            career_stage=stage_default,
            stage_type=None,
        ),
        CareerPath(
            start_date=datetime.date(2023, 3, 15),
            career_stage=stage_contract,
            stage_type="contract",
        ),
        CareerPath(
            start_date=datetime.date(2023, 6, 15),
            career_stage=stage_amendment,
            stage_type="amendment",
        ),
        CareerPath(
            start_date=datetime.date(2023, 8, 15),
            career_stage=stage_amendment,
            stage_type="amendment",
        ),
        CareerPath(
            start_date=datetime.date(2023, 12, 1),
            career_stage=stage_default,
            stage_type=None,
            cae_situation_id=member_cae_situation_option.id,
        ),
    ]
    for cp in carreer_path:
        dbsession.add(cp)
    dbsession.flush()
    return carreer_path


@pytest.fixture
def full_career_path_2(
    dbsession,
    stage_default,
    stage_entry,
    stage_exit,
    integre_cae_situation_option,
    employee_cae_situation_option,
    exit_cae_situation_option,
):
    carreer_path = [
        CareerPath(
            start_date=datetime.date(2023, 4, 1),
            career_stage=stage_default,
            stage_type=None,
            cae_situation_id=integre_cae_situation_option.id,
        ),
        CareerPath(
            start_date=datetime.date(2023, 5, 1),
            career_stage=stage_entry,
            stage_type="entry",
            cae_situation_id=employee_cae_situation_option.id,
        ),
        CareerPath(
            start_date=datetime.date(2023, 9, 1),
            career_stage=stage_exit,
            stage_type="exit",
            cae_situation_id=exit_cae_situation_option.id,
        ),
        CareerPath(
            start_date=datetime.date(2023, 9, 10),
            career_stage=stage_default,
            stage_type=None,
        ),
    ]
    for cp in carreer_path:
        dbsession.add(cp)
    dbsession.flush()
    return carreer_path


@pytest.fixture
def userdatas_with_full_career_path_1(
    dbsession,
    user,
    cae_situation_option,
    company,
    company2,
    full_career_path_1,
    social_status_option1,
    social_status_option2,
):
    user.companies = [company, company2]

    result = UserDatas(
        situation_situation=cae_situation_option,
        coordonnees_lastname="LASTNAME 1",
        coordonnees_firstname="Firstname 1",
        coordonnees_email1="userdatas1@test.fr",
        user_id=user.id,
        career_paths=full_career_path_1,
        coordonnees_family_status="isolated",
    )
    result.situation_situation_id = cae_situation_option.id

    ssd = SocialStatusDatas(
        step="entry", userdatas_id=result.id, social_status_id=social_status_option1.id
    )
    result.social_statuses = [ssd]
    ssd = SocialStatusDatas(
        step="today", userdatas_id=result.id, social_status_id=social_status_option1.id
    )
    ssd2 = SocialStatusDatas(
        step="today", userdatas_id=result.id, social_status_id=social_status_option2.id
    )
    result.today_social_statuses = [ssd, ssd2]

    cf = UserDatasCustomFields(id=result.id)
    result.custom_fields = cf
    result.custom_fields.exp__competences = "Origami"

    dbsession.add(result)
    dbsession.flush()
    user.userdatas = result
    return result


@pytest.fixture
def userdatas_with_full_career_path_2(
    dbsession, user2, cae_situation_option, full_career_path_2
):
    result = UserDatas(
        situation_situation=cae_situation_option,
        coordonnees_lastname="LASTNAME 2",
        coordonnees_firstname="Firstname 2",
        coordonnees_email1="userdatas2@test.fr",
        user_id=user2.id,
        career_paths=full_career_path_2,
    )
    result.situation_situation_id = cae_situation_option.id
    dbsession.add(result)
    dbsession.flush()
    user2.userdatas = result
    return result
