import Radio from 'backbone.radio';
import {
    formatAmount,
} from '../../math.js';
import BaseModel from '../../base/models/BaseModel.js';

const PostTTCLineModel = BaseModel.extend({
    props: [
        'id',
        'label',
        'amount',
    ],
    validation: {
        label: {
            required: true,
            msg: "Veuillez saisir un libellé",
        },
        amount: {
            required: true,
            pattern: "amount",
            msg: "Veuillez saisir un montant, dans la limite de 5 chiffres après la virgule",
        },
    },
    initialize() {
        this.user_session = Radio.channel('session');
        this.on('saved', this.onSaved, this);
        this.user_preferences = Radio.channel('user_preferences');
    },
    amount_label() {
        return formatAmount(this.get('amount'));
    }
});
export default PostTTCLineModel;
