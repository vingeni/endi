/*
 * File Name : ProductCollection.js
 */
import OrderableCollection from 'base/models/OrderableCollection.js';
import ProductModel from './ProductModel.js';
import WorkModel from './WorkModel.js';
import Radio from 'backbone.radio';
import _ from 'underscore';


const ProductCollection = OrderableCollection.extend({
    model(modeldict, options) {
        if (modeldict.type_ == 'progress_invoicing_work') {
            return new WorkModel(modeldict, options);
        } else {
            return new ProductModel(modeldict, options);
        }
    },
});
export default ProductCollection;