import Mn from 'backbone.marionette';
import SelectWidget from '../../../widgets/SelectWidget.js';
import FormBehavior from '../../../base/behaviors/FormBehavior.js';
import PaymentLineTableView from './PaymentLineTableView.js';
import Radio from 'backbone.radio';
import Validation from 'backbone-validation';


var template = require("./templates/PaymentBlockView.mustache");

const PaymentBlockView = Mn.View.extend({
    behaviors: [FormBehavior],
    tagName: 'div',
    className: 'separate_block border_left_block',
    template: template,
    regions: {
        payment_display: '.payment_display-container',
        payment_times: '.payment_times-container',
        deposit: '.payment-deposit-container',
        lines: '.payment-lines-container',
    },
    modelEvents: {
        'sync': 'showModelSection',
        'saved:payment_times': 'onPaymentTimesChanged'
    },
    childViewEvents: {
        'finish': 'onFinish',
    },
    initialize: function (options) {
        this.collection = options['collection'];
        var channel = Radio.channel('config');
        this.payment_display_options = channel.request(
            'get:options',
            'payment_displays'
        );
        this.deposit_options = channel.request(
            'get:options',
            'deposits',
        );
        this.payment_times_options = channel.request(
            'get:options',
            'payment_times'
        );
        channel = Radio.channel('facade');
        this.listenTo(channel, 'bind:validation', this.bindValidation);
        this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    },
    bindValidation() {
        Validation.bind(this);
    },
    unbindValidation() {
        Validation.unbind(this);
    },
    onFinish(key, value) {
        this.triggerMethod('data:persist', key, value);
    },
    renderTable: function () {
        const tableView = new PaymentLineTableView({
            collection: this.collection,
            model: this.model,
        });
        this.showChildView('lines', tableView);
    },
    showPaymentDisplaySelect() {
        this.showChildView(
            'payment_display',
            new SelectWidget({
                options: this.payment_display_options,
                title: "Affichage des paiements",
                field_name: 'paymentDisplay',
                id_key: 'value',
                value: this.model.get('paymentDisplay'),
            })
        );
    },
    showDepositSelect() {
        this.showChildView(
            'deposit',
            new SelectWidget({
                options: this.deposit_options,
                title: "Acompte à la commande",
                field_name: 'deposit',
                id_key: 'value',
                value: this.model.get('deposit'),
            })
        );
    },
    showPaymentTimesSelect() {
        this.showChildView(
            'payment_times',
            new SelectWidget({
                options: this.payment_times_options,
                title: "Paiement en",
                field_name: 'payment_times',
                id_key: 'value',
                value: this.model.get('payment_times'),
            })
        );
    },
    showModelSection() {
        this.showPaymentDisplaySelect();
        this.showDepositSelect();
        this.showPaymentTimesSelect();
    },
    onRender: function () {
        this.showModelSection();
        this.renderTable();
    }
});
export default PaymentBlockView;