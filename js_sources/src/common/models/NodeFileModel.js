/*
 * File Name :  NodeFileModel
 */
import BaseModel from 'base/models/BaseModel';

const NodeFileModel = BaseModel.extend({
    itemBaseUrl: "/api/v1/files/"
});
export default NodeFileModel;
