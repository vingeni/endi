import CustomerApiService from './CustomerApiService'
import ProjectApiService from './ProjectApiService'
import TaskApiService from './TaskApiService'
import NotificationApiService from './NotificationApiService'
import FileApiService from './FileApiService'
import IndicatorApiService from './IndicatorApiService'
import BusinessApiService from './BusinessApiService'

const api = {
  customers: new CustomerApiService(),
  files: new FileApiService(),
  indicators: new IndicatorApiService(),
  notifications: new NotificationApiService(),
  projects: new ProjectApiService(),
  tasks: new TaskApiService(),
  businesses: new BusinessApiService(),
}
export default api
