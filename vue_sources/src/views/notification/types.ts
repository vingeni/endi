type NotificationType = {
  id: number
  key: string
  title: string
  body: string
  due_datetime: string
  user_id: number
  status_type: string
}
