      "changelog": [
        {
          "title": "TITRE",
          "description": [
            "DESCRIPTION_LIGNE_1",
            "DESCRIPTION_LIGNE_2"
          ],
          "category": "enhancement/bugfix",
          "sponsors": [
            "CPE",
            "Collectif MoOGLi"
          ]
        }
      ]
